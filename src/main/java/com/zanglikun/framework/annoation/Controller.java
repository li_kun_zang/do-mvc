package com.zanglikun.framework.annoation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author : zanglikun
 * @date : 2021/2/23 11:50
 * @Version: 1.0
 * @Desc : 自定义的 Controller 注解 需要声明 他的运行周期，我们采用元注解 进行 注解 Controller 注解  如果 这里 不理解 ，请去访问 https://www.zanglikun.com/1078.html 学习
 */

@Target(ElementType.TYPE)   // 设置 只能修饰在类上
@Retention(RetentionPolicy.RUNTIME) // 设置 在运行期有效
public @interface Controller {

    /**
     * @Target(ElementType.METHOD)
     * @Retention(RetentionPolicy.SOURCE)
     */
    // @Override   //我们进入 此注解，获取它的 注解格式，我们修改即可
}
