package com.zanglikun.framework.annoation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author : zanglikun
 * @date : 2021/2/23 11:54
 * @Version: 1.0
 * @Desc : RequestMapping 注解 并加入一个 value 值。
 */
@Target({ElementType.METHOD,ElementType.TYPE}) // 注解在 类和方法上
@Retention(RetentionPolicy.RUNTIME) // 注解此注解 只在运行期
public @interface RequestMapping {
    String value() default ""; // 设置注解 可传递的参数，默认值是 ""
}

