package com.zanglikun.framework.utils;

import org.apache.commons.beanutils.Converter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author : zanglikun
 * @date : 2021/2/25 16:43
 * @Version: 1.0
 * @Desc : 费劲，没啥好说的
 */
public class DateTimeConverter implements Converter {

    // 重写方法
    @Override
    public Object convert(Class aClass, Object o) {
        if (o == null || "".equals(o)){
            return null;
        }
        if (o instanceof java.lang.String){
            String value = o.toString().trim();
            if(aClass.equals(LocalDate.class)){
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                return LocalDate.parse(value,dateTimeFormatter);
            }
            if(aClass.equals(LocalDateTime.class)){
                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                return LocalDateTime.parse(value,dateTimeFormatter);
            }

        }
        return o;
    }
}
