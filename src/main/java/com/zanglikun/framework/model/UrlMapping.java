package com.zanglikun.framework.model;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * @author : zanglikun
 * @date : 2021/2/23 14:04
 * @Version: 1.0
 * @Desc : UrlMapping实体类
 */
public class UrlMapping {
    private Object obj;
    private Method method;
    // String 代表参数名称 Class<?> 代表字节码信息
    private Map<String,Class<?>> parameter;

    public UrlMapping() {
    }

    public UrlMapping(Object obj, Method method, Map<String, Class<?>> parameter) {
        this.obj = obj;
        this.method = method;
        this.parameter = parameter;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Map<String, Class<?>> getParameter() {
        return parameter;
    }

    public void setParameter(Map<String, Class<?>> parameter) {
        this.parameter = parameter;
    }
}
