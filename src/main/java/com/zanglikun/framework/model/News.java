package com.zanglikun.framework.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author : zanglikun
 * @date : 2021/2/25 15:44
 * @Version: 1.0
 * @Desc : 新闻测试类
 */
public class News {
    private Integer id;
    private String title;
    private LocalDate localDate;
    private LocalDateTime localDateTime;

    public News() {
    }

    public News(Integer id, String title, LocalDate localDate, LocalDateTime localDateTime) {
        this.id = id;
        this.title = title;
        this.localDate = localDate;
        this.localDateTime = localDateTime;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
