package com.zanglikun.framework.controller;

import com.zanglikun.framework.annoation.Controller;
import com.zanglikun.framework.annoation.RequestMapping;

/**
 * @author : zanglikun
 * @date : 2021/2/23 12:27
 * @Version: 1.0
 * @Desc : 用户控制类
 */
@Controller
@RequestMapping("/user")
public class UserController {

    @RequestMapping("/add")
    public void ADD(){
        System.out.println("UserController 执行了add方法");
    }

    @RequestMapping("/del")
    public void DEL(){
        System.out.println("UserController 执行了del方法");
    }

    @RequestMapping("/get")
    public void GET(){
        System.out.println("UserController 执行了get方法");
    }


    public void NoScanMethod(){
        System.out.println("UserController 执行了NoScanMethod方法");
    }

}
