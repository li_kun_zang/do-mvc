package com.zanglikun.framework.controller;

import com.zanglikun.framework.annoation.Controller;
import com.zanglikun.framework.annoation.RequestMapping;
import com.zanglikun.framework.model.News;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * @author : zanglikun
 * @date : 2021/2/23 12:27
 * @Version: 1.0
 * @Desc : 新闻控制类
 */
@Controller
@RequestMapping("/news")
public class NewsController {


    @RequestMapping("/add")
    public String ADD(HttpServletRequest request, HttpServletResponse response){
        System.out.println("request"+request);
        System.out.println("response"+response);
        //System.out.println("name"+name);
        System.out.println("NewsController 执行了add方法");
        return "redirect:/indexA.html";

    }

    @RequestMapping("/del")
    public String DEL(){
        System.out.println("NewsController 执行了del方法");
        return "news_del";
    }

    @RequestMapping("/get")
    public Map<String,Object> GET(){
        System.out.println("NewsController 执行了get方法");
        Map<String,Object> map = new HashMap<>();
        map.put("姓名","张三");
        map.put("年龄","80");
        map.put("爱好",new String[]{"抽烟","喝酒","烫头"});
        return map;

    }

    @RequestMapping("/put")
    public Map<String,Object> PUT(String string, Boolean bo, Short s, Byte by, Long l, Float f, Double d, Character c, Integer i, LocalDate localDate, LocalDateTime localDateTime){
        System.out.println("NewsController 执行了PUT方法");
        Map<String,Object> map = new HashMap<>();
        map.put("String",string);
        map.put("Boolean",bo);
        map.put("Short",s);
        map.put("Byte",by);
        map.put("Long",l);
        map.put("Float",f);
        map.put("Double",d);
        map.put("Character",c);
        map.put("Integer",i);
        map.put("LocalDate",localDate);
        map.put("LocalDateTime",localDateTime);
        return map;

    }

    @RequestMapping("/head")
    public Map<String,Object> HEAD(News news){
        System.out.println("NewsController 执行了head方法");
        Map<String,Object> map = new HashMap<>();
        if(news != null){
            map.put("ID",news.getId());
            map.put("Title",news.getTitle());
            map.put("LocalDate",news.getLocalDate());
            map.put("LocalDateTine",news.getLocalDateTime());
        }
        return map;
    }

    @RequestMapping("/post")
    public Map<String,Object> post(String[] string, Boolean[] bo, Short[] s, Byte[] by, Long[] l, Float[] f, Double[] d, Character[] c, Integer[] i, LocalDate[] localDate, LocalDateTime[] localDateTime){
        System.out.println("NewsController 执行了post方法");
        Map<String,Object> map = new HashMap<>();
        map.put("String",string);
        map.put("Boolean",bo);
        map.put("Short",s);
        map.put("Byte",by);
        map.put("Long",l);
        map.put("Float",f);
        map.put("Double",d);
        map.put("Character",c);
        map.put("Integer",i);
        map.put("LocalDate",localDate);
        map.put("LocalDateTime",localDateTime);
        return map;
    }
}
